//
// Created by rnicholl on 1/21/22.
//

#ifndef RPNX_CRYPTO_BLAKE2B_HPP
#define RPNX_CRYPTO_BLAKE2B_HPP

#include <array>
#include <cinttypes>
#include <bit>


namespace rpnx::crypto
{
    struct blake2b_calcstate
    {
        std::array<std::uint64_t, 8> h;
        std::array<std::uint64_t, 16> v;
    };

    inline void blake2b_mix(
                     std::uint64_t &a_o, std::uint64_t &b_o, std::uint64_t &c_o, std::uint64_t &d_o,
                     std::uint64_t x, std::uint64_t y
                     )
    {
        std::uint64_t a = a_o;
        std::uint64_t b = b_o;
        std::uint64_t c = c_o;
        std::uint64_t d = d_o;

        a = a + b + x;
        d = std::rotr(d ^ a, 32);

        c = c + d;
        b = std::rotr((b ^ c), 24);

        a = a + b + y;
        d = std::rotr(d ^ a, 16);

        c = c + d;
        b = std::rotr(b ^ c, 63);

        a_o = a;
        b_o = b;
        c_o = c;
        d_o = d;

    }

    template <std::size_t N = 512>
    std::array<std::byte, N> blake2b_hash();
}

#endif //RPNX_CRYPTO_BLAKE2B_HPP
