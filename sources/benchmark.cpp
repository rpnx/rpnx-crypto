//
// Created by rnicholl on 1/21/22.
//
#include <benchmark/benchmark.h>
#include "sodium.h"
#include <random>
#include <vector>
#include <utility>
#include "rpnx/xsalsa.hpp"


static void bm_xslasa20_sodium(benchmark::State& state)
{

    std::mt19937 prand(0);

    std::vector<std::byte> crypto_nonce;
    crypto_nonce.resize(crypto_stream_xsalsa20_NONCEBYTES);
    static_assert(crypto_stream_xsalsa20_NONCEBYTES == 24);


    std::vector<std::byte> crypto_key;
    crypto_key.resize(crypto_stream_xsalsa20_KEYBYTES);
    static_assert(crypto_stream_xsalsa20_KEYBYTES == 32);

    std::size_t data_len = 1472;
    std::vector<std::byte> data_in(data_len);
    std::vector<std::byte> crypt_out_ref(data_len);
    std::vector<std::byte> crypt_out_impl(data_len);

    for (std::byte & x : crypto_nonce) x = (std::byte) (prand() & 0xFF);
    for (std::byte & x : crypto_key) x = (std::byte) (prand() & 0xFF);
    for (std::byte & x : data_in) x = (std::byte) 0;
    std::vector<std::byte> crypto_nonce_copy = crypto_nonce;







    for (auto _ : state)
    {
        benchmark::ClobberMemory();
        ::crypto_stream_xsalsa20_xor_ic(reinterpret_cast<unsigned char *>(crypt_out_ref.data()),
                                        reinterpret_cast<const unsigned char *>(data_in.data()),
                                        crypt_out_ref.size(),
                                        reinterpret_cast<const unsigned char *>(crypto_nonce.data()),
                                        0,
                                        reinterpret_cast<const unsigned char *>(crypto_key.data()));
        benchmark::ClobberMemory();
        benchmark::DoNotOptimize(crypt_out_ref.data());
    }
}
BENCHMARK(bm_xslasa20_sodium);

static void bm_xslasa20_rpnx(benchmark::State& state)
{

    std::mt19937 prand(0);

    std::vector<std::byte> crypto_nonce;
    crypto_nonce.resize(crypto_stream_xsalsa20_NONCEBYTES);
    static_assert(crypto_stream_xsalsa20_NONCEBYTES == 24);


    std::vector<std::byte> crypto_key;
    crypto_key.resize(crypto_stream_xsalsa20_KEYBYTES);
    static_assert(crypto_stream_xsalsa20_KEYBYTES == 32);

    std::size_t data_len = 1472;
    std::vector<std::byte> data_in(data_len);
    std::vector<std::byte> crypt_out_ref(data_len);
    std::vector<std::byte> crypt_out_impl(data_len);

    for (std::byte & x : crypto_nonce) x = (std::byte) (prand() & 0xFF);
    for (std::byte & x : crypto_key) x = (std::byte) (prand() & 0xFF);
    for (std::byte & x : data_in) x = (std::byte) 0;
    std::vector<std::byte> crypto_nonce_copy = crypto_nonce;

    for (auto _ : state)
    {
        benchmark::ClobberMemory();
        rpnx::crypto::xsalsa<20>(data_in.begin(),  data_in.end(), crypt_out_impl.begin(), crypto_nonce_copy.data(), crypto_key.data() );
        benchmark::ClobberMemory();
        benchmark::DoNotOptimize(crypt_out_ref.data());
    }
}
BENCHMARK(bm_xslasa20_rpnx);
//